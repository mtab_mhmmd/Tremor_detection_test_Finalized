from lithic import *
import asyncio
import json
import numpy as n
import scipy.signal as sg
import matplotlib.pyplot as mpl
import matplotlib.image as img
from FFT_plot import *
from Live_plot import data_collection


# Detecting indications of resting hand tremors - a symptom indicating possibility of Parkinson's


async def sprint12(lithic):
    # Connect to the sensor system
    res = await lithic.connectToServer()
    print(f'Sensor {res}')

    # Quit program if connection is not established
    if res == "Server unreachable":
        exit("\nPlease connect sensor hub AND run Lithic One application.")

    while True:
        # Original prompt loop + live plot loop
        var = True
        while var:
            live_plot_prompt = input("\nThis program will try to detect resting hand tremors."
                                     "\nWould you like to try the live plotter to visualize how the sensors track "
                                     "movements."
                                     "\nIf so, you will have to restart the program for the Tremor Detection Test."
                                     "\nIf you want to start the live plotter, enter 1."
                                     "\nOR"
                                     "\nWould you like to begin the Tremor Detection Test."
                                     "\nIf so, you will have to restart the program for the live plotter."
                                     "\nIf you want to start the Tremor Detection Test, press enter.\n")
            if live_plot_prompt == "1":
                axis_input = int(input("\nFor which axis would you like to visualize how the sensors track movements?"
                                       "\n1. x-axis"
                                       "\n2. y-axis"
                                       "\n3. z-axis"
                                       "\nPlease enter your choice (Wrong input will end the program): "))
                if axis_input <= 0 or axis_input > 4:
                    exit("\nUnexpected Input, please restart program.\n\n")
                else:
                    await data_collection(int(axis_input))
            elif live_plot_prompt != "":
                continue
            else:
                var = False

        # Start Tremor Detection Test (Loop)
        var2 = True
        while var2:
            # Message and photo shown to user for correct orientation of sensor
            plt.close()
            image = img.imread("orientation_img.png")
            plt.imshow(image)
            plt.subplots_adjust(bottom=0.4)
            plt.xlabel("\nAs shown in the image, "
                       "please place the sensor flat on the patient's palm \nsuch that the palm is facing up "
                       "\nand the patient's hand is resting on a flat surface or on the patient's lap."
                       "\nThen close their fingers so as to hold the sensor in place."
                       "\nThen begin the program by pressing enter and wait 5 seconds to see the results."
                       "\nClose Image to continue")
            plt.show()
            user_message = input("\nAs shown in the image, "
                                 "please place the sensor flat on the patient's palm \nsuch that the palm is facing up "
                                 "\nand the patient's hand is resting on a flat surface or on the patient's lap."
                                 "\nThen close their fingers so as to hold the sensor in place."
                                 "\nThen begin the program by pressing enter and wait 5 seconds to see the results."
                                 "\n(Enter to start)")
            if user_message != "":
                continue
            else:
                # Start recording stream of data from sensor
                await lithic.startRecording()

                # Wait for record time = 5 seconds
                await asyncio.sleep(5)

                # End recording and ask for recorded data
                res = await lithic.stopRecording()

                # Decode Json response
                data = json.loads(res)

                # Extract required information from the recorded data
                sample_number, x_acc, y_acc, z_acc, x_gyro, y_gyro, z_gyro = [], [], [], [], [], [], []
                for frame in data:
                    for sample in frame['samples']:
                        # Store data from sample number
                        sample_number = n.append(sample_number, float(sample.split(',')[0]))
                        # Record sensor movement data from all  accelerometer and gyroscope axes
                        x_acc = n.append(x_acc, float(sample.split(',')[1]))
                        y_acc = n.append(y_acc, float(sample.split(',')[2]))
                        z_acc = n.append(z_acc, float(sample.split(',')[3]))
                        x_gyro = n.append(x_gyro, float(sample.split(',')[4]))
                        y_gyro = n.append(y_gyro, float(sample.split(',')[5]))
                        z_gyro = n.append(z_gyro, float(sample.split(',')[6]))

                # Using filter for accelerometer
                # (filter fs = 250 Hz, fc = 20 Hz)
                [bla, ala] = sg.butter(10, (2 * 20) / 250, analog=False, btype='low')
                filtered_x_acc = sg.lfilter(bla, ala, x_acc)
                filtered_y_acc = sg.lfilter(bla, ala, y_acc)
                filtered_z_acc = sg.lfilter(bla, ala, z_acc)

                # Using filter with lower cutoff frequency for gyroscope, since it is more sensitive
                # (filter fs = 250 Hz, fc = 15 Hz)
                [blg, alg] = sg.butter(10, (2 * 15) / 250, analog=False, btype='low')
                filtered_x_gyro = sg.lfilter(blg, alg, x_gyro)
                filtered_y_gyro = sg.lfilter(blg, alg, y_gyro)
                filtered_z_gyro = sg.lfilter(blg, alg, z_gyro)

                # Calculating the pythagorean sum of all the accelerometer and all the gyroscope axes separately
                pyt_sum_acc, pyt_sum_gyr = [], []
                for ind in range(0, len(x_acc)):
                    pyt_sum_acc = n.append(pyt_sum_acc, n.sqrt(n.square(filtered_x_acc[ind])
                                                               + n.square(filtered_y_acc[ind])
                                                               + n.square(filtered_z_acc[ind])))

                for ind in range(0, len(x_gyro)):
                    pyt_sum_gyr = n.append(pyt_sum_gyr, n.sqrt(n.square(filtered_x_gyro[ind])
                                                               + n.square(filtered_y_gyro[ind])
                                                               + n.square(filtered_z_gyro[ind])))

                print("\nTime Domain Analysis:")

                # Time domain analysis on combined accelerometer signal
                above_threshold_count_acc, signal_mean_acc, sigma_acc = time_domain_analysis(pyt_sum_acc, 0.125)
                print("Number of times combined accelerometer signal exceeds threshold in 5 seconds = ",
                      above_threshold_count_acc)
                # If the number of peaks > 14 (i.e., roughly exceeding or at 3 Hz) label it resting tremors
                if above_threshold_count_acc >= 14:
                    print("\tThis implies there are indications of resting tremors.")
                # If the number of peaks < 14 (i.e., roughly less than 2.9-2.8 Hz) label as low to no tremors
                else:
                    print("\tThis implies there are low to no indications of tremors.")

                # Time domain analysis on combined gyroscope signal
                above_threshold_count_gyr, signal_mean_gyr, sigma_gyr = time_domain_analysis(pyt_sum_gyr, 10)
                print("Number of times combined gyroscope signal exceeds threshold in 5 seconds = ",
                      above_threshold_count_gyr)
                # If the number of peaks > 14 (i.e., roughly exceeding or at 3 Hz) label it resting tremors
                if above_threshold_count_gyr >= 14:
                    print("\tThis implies there are indications of resting tremors.")
                # If the number of peaks < 14 (i.e., roughly less than 2.9-2.8 Hz) label as low to no tremors
                else:
                    print("\tThis implies there are low to no indications of tremors.")

                print("\nFrequency Domain Analysis:")

                # Frequency domain analysis on combined accelerometer signal
                f_acc, mag_acc, f_peak_acc, mag_f_peak_acc = frequency_domain_analysis(pyt_sum_acc)

                # Use the FFT peak frequency calculation to inform the user of approximate tremor frequency
                # If peak frequency is above 3 Hz label as tremors, or else label as low to no tremors
                print("As per the combined accelerometer signal:")
                if f_peak_acc >= 3:
                    print("\tTremors are at approximately ", round(f_peak_acc, 1), " Hz")
                    print("\tThis implies there are indications of resting tremors")
                elif 2.5 <= f_peak_acc < 3:
                    print("\tTremors are in the range of 2.5 Hz to 3.0 Hz")
                    print("\tThis implies there are low to no indications of tremors")
                else:
                    print("\tTremors are in the range of 0.0 Hz to 2.5 Hz")
                    print("\tThis implies there are low to no indications of tremors")

                # Frequency domain analysis on combined accelerometer signal
                f_gyr, mag_gyr, f_peak_gyr, mag_f_peak_gyr = frequency_domain_analysis(pyt_sum_acc)

                # Use the FFT peak frequency calculation to inform the user of approximate tremor frequency
                # If peak frequency is above 3 Hz label as tremors, or else label as non-definitive
                print("As per the combined gyroscope signal:")
                if f_peak_gyr >= 3:
                    print("\tTremors are at approximately ", round(f_peak_gyr, 1), " Hz")
                    print("\tThis implies there are indications of resting tremors")
                elif 2.5 <= f_peak_gyr < 3:
                    print("\tTremors are in the range of 2.5 Hz to 3.0 Hz")
                    print("\tThis implies there are low to no indications of tremors")
                else:
                    print("\tTremors are in the range of 0.0 Hz to 2.5 Hz")
                    print("\tThis implies there are low to no indications of tremors")

                # Make subplots to visualize signal calculations
                fig = plt.figure(figsize=(11, 6))
                signal_visual = fig.subplots(3, 2)
                fig.suptitle('Tremor Detection Attempt')
                fig.subplots_adjust(hspace=0.9)

                # Subplot filtered version of the recorded signal from accelerometer and gyroscope
                signal_visual[0, 0].plot(sample_number, filtered_x_acc, c='red')
                signal_visual[0, 0].plot(sample_number, filtered_y_acc, c='blue')
                signal_visual[0, 0].plot(sample_number, filtered_z_acc, c='green')
                signal_visual[0, 0].set_title('Low Pass Filtered Version of Accelerometer Signal')
                signal_visual[0, 0].set_xlabel("Samples")
                signal_visual[0, 1].plot(sample_number, filtered_x_gyro, c='magenta')
                signal_visual[0, 1].plot(sample_number, filtered_y_gyro, c='orange')
                signal_visual[0, 1].plot(sample_number, filtered_z_gyro, c='black')
                signal_visual[0, 1].set_title('Low Pass Filtered Version of Gyroscope Signal')
                signal_visual[0, 1].set_xlabel("Samples")
                fig.legend(labels=["x_acc", "y_acc", "z_acc", "x_gyro", "y_gyro", "z_gyro"], loc='upper right')

                # Subplot pythagorean sum of filtered signal for accelerometer and gyroscope
                signal_visual[1, 0].plot(sample_number, pyt_sum_acc)
                # Add dashed lines to mark threshold
                signal_visual[1, 0].axhline(y=(signal_mean_acc + sigma_acc), linewidth=0.5, color='k', linestyle='--')
                # Add above threshold markers
                above_threshold_acc = pyt_sum_acc > (signal_mean_acc + sigma_acc)
                signal_visual[1, 0].scatter(sample_number[above_threshold_acc], pyt_sum_acc[above_threshold_acc],
                                            color='red', s=5, marker='o')
                signal_visual[1, 0].set_title('Pythagorean Sum of Filtered Accelerometer Signals')
                signal_visual[1, 0].set_xlabel("Samples")

                signal_visual[1, 1].plot(sample_number, pyt_sum_gyr)
                # Add dashed lines to mark threshold
                signal_visual[1, 1].axhline(y=(signal_mean_gyr + sigma_gyr), linewidth=0.5, color='k', linestyle='--')
                # Add above threshold markers
                above_threshold_gyr = pyt_sum_gyr > (signal_mean_gyr + sigma_gyr)
                signal_visual[1, 1].scatter(sample_number[above_threshold_gyr], pyt_sum_gyr[above_threshold_gyr],
                                            color='red', s=5, marker='o')
                signal_visual[1, 1].set_title('Pythagorean Sum of Filtered Gyroscope Signals')
                signal_visual[1, 1].set_xlabel("Samples")

                # Sub plot FFT of pythagorean sum to visualize frequency spectrum
                signal_visual[2, 0].plot(f_acc, mag_acc)
                signal_visual[2, 0].set_ylabel("Magnitude")
                signal_visual[2, 0].set_xlabel("Frequency (Hz)")
                # Add marker for Peak FFT frequency
                signal_visual[2, 0].scatter(f_peak_acc, mag_f_peak_acc, marker='x', c='red')
                signal_visual[2, 0].set_xlim(-0.5, 30)
                signal_visual[2, 0].set_title('FFT Plot of Pythagorean Summed Accelerometer Signal')

                signal_visual[2, 1].plot(f_gyr, mag_gyr)
                signal_visual[2, 1].set_ylabel("Magnitude")
                signal_visual[2, 1].set_xlabel("Frequency (Hz)")
                # Add marker for Peak FFT frequency
                signal_visual[2, 1].scatter(f_peak_gyr, mag_f_peak_gyr, marker='x', c='red')
                signal_visual[2, 1].set_xlim(-0.5, 30)
                signal_visual[2, 1].set_title('FFT Plot of Pythagorean Summed Gyroscope Signal')

                mpl.show()

                # Plot spectrogram of pythagorean sum to visualize frequency spectrum relative to time
                fig2 = plt.figure(figsize=(11, 6))
                spectra = fig2.subplots(2)
                fig2.suptitle('Spectrogram of Pythagorean Summed Signals')
                fig2.subplots_adjust(hspace=0.7)

                f, t, Sxx = sg.spectrogram(pyt_sum_acc, 250)
                spectra[0].pcolormesh(t, f, Sxx, shading='gouraud')
                spectra[0].set_ylim(0, 20)
                spectra[0].set_ylabel('Frequency (Hz)')
                spectra[0].set_xlabel('Time (sec)')
                spectra[0].set_title('Combined Accelerometer Frequency vs. Time\n')

                f2, t2, Sxx2 = sg.spectrogram(pyt_sum_gyr, 250)
                spectra[1].pcolormesh(t2, f2/2, Sxx2, shading='gouraud')
                spectra[1].set_ylim(0, 20)
                spectra[1].set_ylabel('Frequency (Hz)')
                spectra[1].set_xlabel('Time (sec)')
                spectra[1].set_title('Combined Gyroscope Frequency vs. Time\n')

                mpl.show()

                # Prompt user to repeat tremor detection test
                continue_prompt = input("\nDo you want to perform another Tremor Detection Test ? \n"
                                        "(Enter n to exit test, any other input to continue) ")
                # If user declines end program
                if continue_prompt == 'n':
                    exit("\nPlease restart program to try live plotter or Tremor Detection Test\n\n")


def time_domain_analysis(pyt_sum, typical_dev):
    # Finding mean and standard deviation of the summed signal to determine threshold
    signal_mean = n.mean(pyt_sum)
    std_deviation = n.std(pyt_sum, ddof=1)
    if std_deviation < typical_dev:  # Ignore standard deviation if too low
        sigma = signal_mean
    else:
        # Or else set sigma threshold to 0.86 * std. deviation
        # (i.e. report anything that is 30.4% above the mean)
        sigma = 0.86 * std_deviation
    # Count number of times signal exceeds above the sigma deviation of the mean
    above_threshold_count = 0
    i = 0
    while i < len(pyt_sum):
        if pyt_sum[i] > (signal_mean + sigma):  # Reach exceed event
            above_threshold_count += 1  # Count it
            i += 1
            for j in range(i, len(pyt_sum)):
                if pyt_sum[j] <= (signal_mean + sigma):
                    break
                # keep incrementing search index till the value is below threshold
                else:
                    i += 1
        else:
            i += 1

    return above_threshold_count, signal_mean, sigma


def frequency_domain_analysis(pyt_sum):
    # Demean the signal before FFT to reduce DC offset
    pyt_sum = pyt_sum - n.mean(pyt_sum)
    # Calculate FFT of the pythagorean sum and create Magnitude(yf) and Frequency(xf) arrays
    yf, xf = fftPlot(pyt_sum, plot=False)

    # Set frequency to scale frequency axis of FFT Plot
    fs = 250

    max_abs_fft = n.amax(n.abs(yf))
    # print(max_abs_fft)

    # If max FFT magnitude is above 1, find its index
    # and set the corresponding index in frequency array to FFT peak frequency
    if max_abs_fft >= 0.07:
        max_ind_fft = n.argwhere(n.abs(yf) == max_abs_fft)[0].flatten()
        xf_max = xf[max_ind_fft]
    else:
        # If max magnitude is below 1, ignore it and set FFT peak frequency to 0
        max_abs_fft = 0
        xf_max = [0.0]

    return xf * fs, n.abs(yf), xf_max[0] * fs, max_abs_fft


async def main():
    await sprint12(Lithic())


asyncio.get_event_loop().run_until_complete(main())
