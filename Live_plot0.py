import time as dt
import json
import scipy.signal as sg
from lithic import *
from FFT_plot import *


# Initial attempt at live plot - discarded due to increasing lag with time
async def live_plot(lithic):
    # Connect to the sensor system
    res = await lithic.connectToServer()
    print(f'Sensor {res}')

    # Quit program if connection is not established
    if res == "Server unreachable":
        exit("\nPlease connect sensor hub AND run Lithic One application.")

    # Create figure for plotting
    fig1 = plt.figure(figsize=(10, 6))
    ax = fig1.subplots(3)
    fig1.subplots_adjust(hspace=0.8)

    count = 0
    x_acc, y_acc, z_acc, x_gyro, y_gyro, z_gyro = np.zeros(1300), np.zeros(1300), np.zeros(1300), np.zeros(
        1300), np.zeros(1300), np.zeros(1300)
    while True:
        start = dt.time()
        res = await lithic.fetchLast("65")
        # Decode Json response
        data = json.loads(res)
        # Extract required information from the recorded data
        i = 0
        for frame in data:
            for sample in frame['samples']:
                # Record sensor movement data from all  accelerometer and gyroscope axes
                x_acc[i] = float(sample.split(',')[1])
                y_acc[i] = float(sample.split(',')[2])
                z_acc[i] = float(sample.split(',')[3])
                x_gyro[i] = float(sample.split(',')[4])
                y_gyro[i] = float(sample.split(',')[5])
                z_gyro[i] = float(sample.split(',')[6])
                i += 1

        # Using filter for accelerometer
        # (filter fs = 250 Hz, fc = 20 Hz)
        [bla, ala] = sg.butter(10, (2 * 20) / 250, analog=False, btype='low')
        filtered_x_acc = sg.lfilter(bla, ala, x_acc)
        filtered_y_acc = sg.lfilter(bla, ala, y_acc)
        filtered_z_acc = sg.lfilter(bla, ala, z_acc)

        # Using filter with lower cutoff frequency for gyroscope, since it is more sensitive
        # (filter fs = 250 Hz, fc = 15 Hz)
        [blg, alg] = sg.butter(10, (2 * 15) / 250, analog=False, btype='low')
        filtered_x_gyro = sg.lfilter(blg, alg, x_gyro)
        filtered_y_gyro = sg.lfilter(blg, alg, y_gyro)
        filtered_z_gyro = sg.lfilter(blg, alg, z_gyro)

        pyt_sum = []
        for ind in range(0, len(x_acc)):
            pyt_sum = np.append(pyt_sum, np.sqrt(np.square(filtered_x_acc[ind]) + np.square(filtered_y_acc[ind])
                                                 + np.square(filtered_z_acc[ind])
                                                 + np.square(filtered_x_gyro[ind]) + np.square(filtered_y_gyro[ind])
                                                 + np.square(filtered_z_gyro[ind])))

        # FFT plot calculations
        # Calculate FFT of the pythagorean sum and create Magnitude(yf) and Frequency(xf) arrays
        yf, xf = fftPlot(pyt_sum[-200:], plot=False)

        # Set sampling frequency to scale frequency axis of FFT Plot with Nyquist frequency, i.e. fs/2
        fs = 250.0
        f_nyq = fs / 2
        # Set start index to search for peak
        # set the index to be after anywhere above or at 2.6 Hz, since tremors typically have low range of about 3Hz
        start_search_ind = next(x[0] for x in enumerate(xf * f_nyq) if x[1] >= 2.6)

        # Find maximum, i.e., peak, of FFT magnitude by beginning search from after DC values
        for_peak = []
        for y in yf[start_search_ind:]:
            # Truncate FFT magnitude from search index to end
            for_peak = np.append(for_peak, np.abs(y))
        # Set maximum in the truncated FFT magnitude array
        max_abs_fft = np.amax(for_peak)
        # print(max_abs_fft)

        # If max FFT magnitude is above 1, find its index
        # and set the corresponding index in frequency array to FFT peak frequency
        if max_abs_fft > 1:
            max_ind_fft = np.argwhere(for_peak == max_abs_fft)[0].flatten()
            xf_max = xf[max_ind_fft + start_search_ind]
        else:
            # If max magnitude is below 1, ignore it and set FFT peak frequency to 0
            max_abs_fft = 0
            xf_max = [0.0]

        # set origin of x axis to be dynamic
        time_axis = np.linspace(count, count + ((len(z_acc) - 1) * (1 / 250)), len(z_acc))
        line1, = ax[0].plot(time_axis, x_acc, c='blue')
        line2, = ax[0].plot(time_axis, y_acc, c='black')
        line3, = ax[0].plot(time_axis, z_acc, c='red')
        ax[0].set_xlim(count, ((len(z_acc) - 1) * (5 / 1280)) + count)
        ax[0].set_title("Accelerometer Signal")

        line4, = ax[1].plot(time_axis, x_gyro, c='green')
        line5, = ax[1].plot(time_axis, y_gyro, c='purple')
        line6, = ax[1].plot(time_axis, z_gyro, c='orange')
        ax[1].set_xlim(count, ((len(z_acc) - 1) * (5 / 1280)) + count)
        ax[1].set_title("Gyroscope Signal")
        ax[1].set_xlabel("Time (sec)")

        fig1.legend(labels=["x_acc", "y_acc", "z_acc", "x_gyro", "y_gyro", "z_gyro"], loc="right")

        # Sub plot FFT of pythagorean sum to visualize frequency spectrum
        line7, = ax[2].plot(xf * f_nyq, np.abs(yf))
        ax[2].set_ylabel("Magnitude")
        ax[2].set_xlabel("Frequency (Hz)")
        # Add marker for Peak FFT frequency
        ax[2].scatter(xf_max[0] * f_nyq, max_abs_fft, marker='x', c='red')
        ax[2].set_ylim(0, max_abs_fft + 2.5)
        ax[2].set_title('FFT Plot of Pythagorean Summed Signal')

        while dt.time() - start < 0.5:
            line1.set_ydata(x_acc)
            line2.set_ydata(y_acc)
            line3.set_ydata(z_acc)
            line4.set_ydata(x_gyro)
            line5.set_ydata(y_gyro)
            line6.set_ydata(z_gyro)
            line7.set_ydata(np.abs(yf))
            plt.pause(0.1)

        ax[0].clear()
        ax[1].clear()
        ax[2].clear()
        end = dt.time()
        count = count + float(end - start)
        print(float(end - start))


async def main():
    await live_plot(Lithic())


asyncio.run(main())
