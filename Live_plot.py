# import time as dt
import json
import math
from lithic import *
from FFT_plot import *


# Function to initiate data collection for live plotting
async def data_collection(axis_input):
    await to_plot(Lithic(), axis_input)


# Function to continuously collect sensor data and plot it iteratively via live_plot function
async def to_plot(lithic, axis_input):
    # Connect to the sensor system
    res = await lithic.connectToServer()
    # print(f'Sensor {res}')

    # Quit program if connection is not established
    if res == "Server unreachable":
        exit("\nPlease connect sensor hub AND run Lithic One application.")

    # Fix memory allocation for the arrays that take in live data
    x_acc, y_acc, z_acc, x_gyro, y_gyro, z_gyro, sample_number = np.zeros(1300), np.zeros(1300), np.zeros(1300), \
                                                                 np.zeros(1300), np.zeros(1300), np.zeros(1300), \
                                                                 np.zeros(1300)

    # Input prompt for duration of live plot
    duration = int(input("\nHow long would you like to see the live plot ? (Input has to be integer > 0): "))
    # End program if duration is less than 10 seconds
    if duration <= 0:
        exit("\nDuration has to be greater than 0 seconds\n\n")

    frame_count = 0
    # Start data collection + live plot loop
    while True:
        # start = dt.time()
        var = True

        # Start data collection (Loop)
        while var:
            # Fetch sensor data of approximately the last five seconds
            res = await lithic.fetchLast("65")

            # Decode Json response
            data = json.loads(res)

            # Extract required information from the recorded data
            i = 0
            for frame in data:
                for sample in frame['samples']:
                    # Record sensor movement data from all  accelerometer and gyroscope axes
                    sample_number[i] = ((i + frame_count) / 260) - 5  # float(sample.split(',')[0])
                    x_acc[i] = float(sample.split(',')[1])
                    y_acc[i] = float(sample.split(',')[2])
                    z_acc[i] = float(sample.split(',')[3])
                    x_gyro[i] = float(sample.split(',')[4])
                    y_gyro[i] = float(sample.split(',')[5])
                    z_gyro[i] = float(sample.split(',')[6])
                    i += 1
                    # file.write(sample)
                    # file.write("\n")
                frame_count += 1
                # print(sample_number)
            # file.close()
            var = False
        # end = dt.time()
        # print(end - start)

        # Call live plot depending on which axis the user wants to visualize
        if axis_input == 1:
            live_plot(sample_number, x_acc, x_gyro, "X-Axis")
        elif axis_input == 2:
            live_plot(sample_number, y_acc, y_gyro, "Y-Axis")
        elif axis_input == 3:
            live_plot(sample_number, z_acc, z_gyro, "Z-Axis")
        else:
            exit("\nUnexpected Input, please restart program.2\n\n")

        # If duration of live plot reached, prompt user if they want to continue
        if math.floor(sample_number[-1]) == duration:
            continue_prompt = input("\nDo you want to keep using the live plotter ? \n"
                                    "(Enter n to exit live plot, any other input to continue) ")
            # If user chooses not to continue, end program
            if continue_prompt == 'n':
                exit("\nPlease restart program to try live plotter or Tremor Detection Test\n\n")
            else:
                # Else prompt user for new duration and restart plot
                duration = int(input("\nHow long would you like to see the live plot again? "
                                     "(Input has to be integer > 0): "))
                frame_count = 0
                # End program if duration is less than 1 second
                if duration <= 0:
                    exit("\nDuration has to be greater than 0 seconds\n\n")


# Create figure for plotting
fig = plt.figure(figsize=(8, 6))
ax = fig.subplots(2)
fig.subplots_adjust(hspace=0.8)


# Function for live plotting accelerometer and gyroscope data of user chosen axis
def live_plot(sample_number, acc, gyro, title):
    # start = dt.time()
    # Pause for 0.01 second after every iteration
    plt.pause(0.01)

    # Clear plot of previous iteration and plot collected accelerometer data of current iteration
    ax[0].clear()
    ax[0].plot(sample_number, acc, c='blue')
    ax[0].set_title(title + " Accelerometer Signal")
    ax[0].set_xlabel("Time (sec)")

    # Clear plot of previous iteration and plot collected gyroscope data of current iteration
    ax[1].clear()
    ax[1].plot(sample_number, gyro, c='green')
    ax[1].set_title(title + " Gyroscope Signal")
    ax[1].set_xlabel("Time (sec)")
    # end = dt.time()
    # print(end-start)

# asyncio.run(data_collection(2))
