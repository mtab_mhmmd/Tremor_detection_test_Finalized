#LithicOne v1.0.6

import asyncio

class Lithic:
			
	async def connectToServer(self):
		try:
			self.reader, self.writer = await asyncio.open_connection('127.0.0.1', 3333)
			return "Connection successful"
		except ConnectionRefusedError:
			return "Server unreachable"

	async def startLive(self):
		try:
			message = "0:"
			self.writer.write(message.encode())
			await self.writer.drain()
			return "Call getLive() continuously"
		except AttributeError:
			return "Connect to server first"

	async def stopLive(self):
		try:
			message = "1:"
			self.writer.write(message.encode())
			await self.writer.drain()
			return data.decode()[:-1]
		except AttributeError:
			return "Connect to server first"

	async def getLive(self):
		try:
			data = await self.reader.readuntil(b'\4')
			return data.decode()[:-1]
		except AttributeError:
			return "Connect to server and use startLive first"

	async def startRecording(self):
		try:
			message = "2:"
			self.writer.write(message.encode())
			await self.writer.drain()

			data = await self.reader.readuntil(b'\4')
			return data.decode()[:-1]
		except AttributeError:
			return "Connect to server first"

	async def stopRecording(self):
		try:
			message = "3:"
			self.writer.write(message.encode())
			await self.writer.drain()

			data = ""
			complete = False
			while(not complete):
				try:
					data += (await self.reader.readuntil(b'\4')).decode()
					complete = True
				except asyncio.exceptions.LimitOverrunError as err:
					# print(err.consumed)
					data += (await self.reader.read(err.consumed)).decode()
			return data[:-1]
		except AttributeError:
			return "Connect to server first"

	async def fetchLast(self, extent):
		try:
			message = "5:"+extent
			self.writer.write(message.encode())
			await self.writer.drain()
			
			data = ""
			complete = False
			while(not complete):
				try:
					data += (await self.reader.readuntil(b'\4')).decode()
					complete = True
				except asyncio.exceptions.LimitOverrunError as err:
					# print (err.consumed)
					data += (await self.reader.read(err.consumed)).decode()

			return data[:-1]
		except AttributeError:
			return "Connect to server first"

	async def setNodeLed(self, led, on):
		try:
			if((led==1 or led==2) and (on==0 or on==1)):
				message = "6:"+str(led)+","+str(on)
				self.writer.write(message.encode())
				await self.writer.drain()

				data = await self.reader.readuntil(b'\4')
				return data.decode()[:-1]
		except AttributeError:
			return "Connect to server first"

	async def setDockLed(self, r, g, b):
		try:
			if((r>=0 and r<=255) and (g>=0 and g<=255) and (b>=0 and b<=255)):
				message = "7:"+str(r)+","+str(g)+","+str(b)
				self.writer.write(message.encode())
				await self.writer.drain()

				data = await self.reader.readuntil(b'\4')
				return data.decode()[:-1]
		except AttributeError:
			return "Connect to server first"

	async def setChannel(self, channel):
		try:
			if(channel>=1 or channel<=40):
				message = "8:"+str(channel)
				self.writer.write(message.encode())
				await self.writer.drain()

				data = await self.reader.readuntil(b'\4')
				return data.decode()[:-1]
		except AttributeError:
			return "Connect to server first"
